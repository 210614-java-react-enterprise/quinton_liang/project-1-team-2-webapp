package dev.team2.tapeworm;

import dev.team2.daos.tapeworm.SubscriberDaoImpl;
import dev.team2.models.Subscriber;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.SQLException;
import java.util.Date;

import static org.junit.Assert.*;

public class SubscriberDaoImplTest {
    SubscriberDaoImpl subscriberDao = new SubscriberDaoImpl();
    static Subscriber subscriber = new Subscriber();

    public SubscriberDaoImplTest() throws SQLException {
    }

    @BeforeClass
    public static void populateSubscriber(){
        subscriber.setLastName("D");
        subscriber.setFirstName("Leo");
        subscriber.setEmail("leo@d.com");
        subscriber.setAddress("123 Hollywood");
        subscriber.setPhone("1112223333");
        subscriber.setPassword("TITANIC");
    }

    @Test
    public void testAddSubscriber(){
        assertEquals(subscriber, subscriberDao.addSubscriber(subscriber));
    }

    @Test
    public void testDeleteSubscriber(){
        assertEquals(subscriber, subscriberDao.deleteSubscriber(subscriber));
    }

    @Test
    public void testReplacePassword(){
        subscriber.setId(76L);
        String password = "1234";
        assertEquals(subscriber, subscriberDao.replacePassword(subscriber, password));
    }

}
