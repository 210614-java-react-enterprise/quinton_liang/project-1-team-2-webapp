package dev.team2.daos;

import dev.team2.models.Subscriber;

import java.util.List;

/**
 * Subscriber DAO interface.
 */
public interface SubscriberDao {
    public Subscriber addSubscriber(Subscriber subscriber);
    public Subscriber deleteSubscriber(Subscriber subscriber);
    public List<String> getSubscribers();
    public Subscriber replacePassword(Subscriber subscriber, String password);
}
