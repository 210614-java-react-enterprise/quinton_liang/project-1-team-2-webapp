package dev.team2.servlets;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dev.team2.daos.tapeworm.SubscriberDaoImpl;
import dev.team2.models.Subscriber;
import dev.team2.services.SubscriptionService;
import dev.team2.util.Validation;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Subscriber servlet.
 */
public class SubscriberServlet extends HttpServlet {

    private final SubscriptionService subscriptionService =
            new SubscriptionService(new SubscriberDaoImpl());

    public SubscriberServlet() throws SQLException {
        super();
    }

    /**
     * Add a subscriber
     *
     * example postman json:
     * {"id":"", "email":"wyatt@goettsch.com", "lastName":"Goettsch", "firstName":"Wyatt", "birthDate":"1990-01-01", "address":"321 Street", "phone":"14444444444", "password":"5510"}
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

        response.setStatus(400);

        try (
            BufferedReader bufferedReader = request.getReader();
            PrintWriter printWriter = response.getWriter()
        ) {
            String subscriberJson = bufferedReader.readLine();

            // convert json to subscriber object
            ObjectMapper objectMapper = new ObjectMapper();
            Subscriber subscriber = objectMapper.readValue(subscriberJson, Subscriber.class);

            // email format validation
            if (subscriber != null) {
                Validation validation = new Validation();
                if(!validation.verifyEmail(subscriber.getEmail())){
                    response.sendError(400, "email formatting error");
                    return;
                }else if(!validation.verifyPhone(subscriber.getPhone())){
                    response.sendError(400, "phone formatting error");
                    return;
                }
            }

            // use service to add new subscriber
            Subscriber newSubscriber = subscriptionService.add(subscriber);
            if (newSubscriber != null) {
                printWriter.write(objectMapper.writeValueAsString(newSubscriber));
                response.setStatus(201);
                return;
            }

        } catch (Exception e) {
            e.printStackTrace();
            response.sendError(500);
        }

    }

    /**
     * Delete a subscriber.
     *
     * example postman json:
     * {"id":"", "email":"delete@me.com", "lastName":"Goettsch", "firstName":"Wyatt", "birthDate":"1990-01-01", "address":"321 Street", "phone":"14444444444", "password":"5510"}
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setStatus(400);

        try (
                BufferedReader reader = request.getReader();
                PrintWriter printWriter = response.getWriter()
        ) {
            String subscriberJson = reader.readLine();

            //convert json to subscriber object
            ObjectMapper objectMapper = new ObjectMapper();
            Subscriber subscriber = objectMapper.readValue(subscriberJson, Subscriber.class);

            //use service to remove subscriber
            Subscriber newSubscriber = subscriptionService.remove(subscriber);
            if (newSubscriber != null) {
                response.setStatus(204);
                printWriter.write(objectMapper.writeValueAsString(newSubscriber));
                return;
            }

        } catch (Exception e) {
            e.printStackTrace();
            response.sendError(500);
        }

    }

    /**
     * Change a subscriber's password.
     *
     * example postman json:
     * [ {"id":"", "email":"dennis@ritchie.com", "lastName":"", "firstName":"", "birthDate":"", "address":"", "phone":"", "password":"5510"},
     * {"id":"", "email":"dennis@ritchie.com", "lastName":"", "firstName":"", "birthDate":"", "address":"", "phone":"", "password":"5511"} ]
     *
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try (
                BufferedReader reader = request.getReader();
                PrintWriter printWriter = response.getWriter()
        ) {

            response.setStatus(400);

            // Converts JSON lines into objects using the stream API
            ObjectMapper objectMapper = new ObjectMapper();
            List<Subscriber> subscriberList =
                    Arrays.stream(reader.lines().toArray())
                        .map(line -> (String) line)
                        .map(line -> {
                            if (line.startsWith("[")) {
                                line = line.substring(1);
                            }
                            if (line.endsWith("]")) {
                                line = line.substring(0, line.length() - 1);
                            }
                            if (line.endsWith(",")) {
                                line = line.substring(0, line.length() - 1);
                            }
                            return line;
                        })
                        .map(e -> {
                            try {
                                return objectMapper.readValue(e, Subscriber.class);
                            } catch (JsonProcessingException jsonProcessingException) {
                                jsonProcessingException.printStackTrace();
                                return null;
                            }
                        })
                        .collect(Collectors.toList());

            //use service to replace password
            Subscriber updatedSubscriber = subscriptionService
                    .replacePassword(subscriberList.get(0), subscriberList.get(1).getPassword());
            if (updatedSubscriber != null) {
                printWriter.write(objectMapper.writeValueAsString(updatedSubscriber));
                response.setStatus(200);
                return;
            }

        } catch (Exception e) {
            e.printStackTrace();
            response.sendError(500);
        }

    }


    /**
     * Prints a list of all the subscribers in the database.
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setStatus(400);

        // obtain list of email objects from database or other source
        List<String> stringList = subscriptionService.getAll();
        if (stringList == null) {
            response.sendError(500);
            return;
        }

        StringBuilder stringBuilder = new StringBuilder();
        Iterator<String> iterator = stringList.iterator();
        while (iterator.hasNext()) {
            stringBuilder.append("\""+iterator.next()+"\"");
            if (iterator.hasNext()) {
                stringBuilder.append(", ");
            }
        }

        try (PrintWriter printWriter = response.getWriter()) {
            printWriter.write("[ " + stringBuilder + " ]");
            response.setStatus(200);
            return;
        } catch (Exception e) {
            e.printStackTrace();
            response.sendError(500);
        }

    }

}
