package dev.team2.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Validation {
    public boolean verifyPhone(String number){
        char[] digits = number.toCharArray();
        //Checks for length of 10 or 11
        if(digits.length != 10 && digits.length != 11){
            return false;
        }
        //Checks for non digits
        for(char i : digits){
            if(!Character.isDigit(i)){
                return false;
            }
        }
        return true;
    }

    public boolean verifyEmail(String email){
        List<String> specialCharacters = Arrays.asList(new String[]{"!", "#", "$", "%", "^", "&", "*", "(", ")", "+",
        "=", "{", "}", "[", "]", "|", "?", "<", ">"});

        //Makes sure the email contains @
        if(!email.contains("@")){
            return false;
        //Makes sure the @ isn't at the beginning or end
        }else if(email.startsWith("@")){
            return false;
        }else if(email.endsWith("@")){
            return false;
        //Makes sure the email doesn't contain special characters
        } else{
            for(String special : specialCharacters){
                if(email.contains(special)){
                    return false;
                }
            }
        }
        return true;
    }
}
